let arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

let newList = arr.map(function (parent) {
  addToDoItem(parent);
});

function addToDoItem(itemText) {
  const itemElem = document.createElement("li");
  itemElem.style.cssText = `
       color: red;
       font-weight: bold;
       font-size: 22px;
        `;
  itemElem.innerText = itemText;
  document.getElementById("list").append(itemElem);
  itemText = setTimeout(function () {
    itemElem.remove();
  }, 3000);
}
